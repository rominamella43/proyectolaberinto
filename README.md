# Maze
juego de salir de un laberinto, implementado en python 3.8.

### Pre-requisitos 📋

Python 3.8.

### Instalación 🔧

Se debe ejecutar directamente el archivo Maze.py.

## Controles 🕹️

El jugador se controla con las teclas W, S, A, D. 
El menú se controla con las teclas R y S; R para reiniciar y S para salir. 

## Juego 🎮

Un laberinto es una construcción hecha con uno o varios caminos que engañan a quien lo recorre para dificultar el encuentro de la única salida. En este caso el camino esta constituido por 3 y el jugador es un 'O'.  

## Construido con 🛠️

* [Sublime Text 3](https://www.sublimetext.com/3) - Editor de código.
* [Python 3.8](https://www.python.org/) - Lenguaje de programación.

## Autores ✒️

* **Romina Mella** - *Tranajo completo* - [rominamella43](https://gitlab.com/rominamella43)

