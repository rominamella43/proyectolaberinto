import random 
import math 
import copy
import os 

#Creación laberinto en blanco.
def Maze(n):
	laberinto=[] 
	for x in range(n):
		laberinto.append([]) 
		for y in range(n):
			laberinto[x].append(" ")
	laberinto[0][0]= 3
	laberinto[n-1][n-1]= 1
	return laberinto




#Generación del laberinto.
def GeneracionLaberinto(Lab):
	rand = 0
	x = 0
	y = 0
	while (Lab[n-1][n-1] == 1):
		#movimiento derecha y abajo.
		if (x!=n-1) & (y!=n-1):
			rand=random.randint(3,4)
			if rand==3:
				y= y+1
				Lab[y][x] = 3
			if rand==4:
				x= x+1
				Lab[y][x] = 3

			#Movimiento abajo cuando x = n-1.
		if (x==n-1) & (y!=n-1):
			y = y+1
			Lab[y][x] = 3

			#Movimiento derecha cuando y = n-1
		if (x!=n-1)& (y==n-1):
			x= x+1
			Lab[y][x] = 3

#Generador de caminos alternativos
def Alternativos(laberinto,n,x,y):
	rand= 0 #variable para generar movimiento aleatorio.
	limite =0
	while (limite !=1):
		rand=random.randint(0,4)

		#Restriccion de las esquinas.
		if (x==0)&(y==0): 
			limite=1
		if (x==0)&(y==n-1):
			limite=1
		if (x==n-1)&(y==0):
			limite=1
		if (x==n-1)&(y==n-1):
			limite=1

		#Creacion hacia la derecha.
		if (rand==0)&(x!=n-1):
			if laberinto [y][x+1]== " ":
				laberinto[y][x+1]=0
				x=x+1

		#Creacion hacia abajo.
		if (rand==1)&(y!=n-1):
			if laberinto[y+1][x]==" ":
				laberinto[y+1][x]=0
				y=y+1

		#Creacion hacia izquierda.
		if (rand==2)&(x!=0):
			if laberinto[y][x-1]==" ":
				laberinto[y][x-1]=0
				x=x-1

		#Creacion hacia arriba.
		if (rand==3)&(y!=0):
			if laberinto[y-1][x]==" ":
				laberinto[y-1][x]=0
				y=y-1

		#Fin de la creacion.
		if (rand==4):
			limite=1

	return laberinto

def ImprimirMap(Lab,n):
	for x in range(n):
		for y in range(n):
			if Lab[y][x]==0:
				Lab[y][x]=3


def Move(Lab,n):
	historial = 0
	x= 0
	y= 0
	Lab[n-1][n-1] = 6 #Defino como meta.
	Lab[y][x] = 'O' #Posición del jugador.
	stop=0
	opcion = 0
	while stop!=1:
		for a in range(n):	#impresion de laberinto
			for b in range(n):
				print(Lab[a][b], end=" ")
			print("")
		print("Desplazamiento: W,S,A,D")
		entrada = input("accion: ")

		#Movimiento hacia arriba.
		if entrada.upper()== "W":
			if y==0:
				continue
			elif Lab[y-1][x]==3:
				Lab[y-1][x] = 'O'
				Lab[y][x]= 3
				y=y-1
				historial = historial + 1
			else:
				continue

		#Movimiento hacia la derecha
		if entrada.upper()=="D":
			if x==n-1:
				continue
			elif Lab[y][x+1]==3:
				Lab[y][x+1] = 'O'
				Lab[y][x]= 3
				x=x+1
				historial = historial + 1
			elif Lab[y][x+1]== 6:
				historial = historial+1
				stop=1
				opcion = 1
			else :
				continue

		#Movimiento hacia abajo.
		if entrada.upper()=="S":
			if y==n-1:
				continue
			elif Lab[y+1][x]==3:
				Lab[y+1][x] = 'O'
				Lab[y][x]= 3
				y=y+1
				historial= historial+1
			elif Lab[y+1][x]== 6:
				historial = historial+1
				stop=1
				opcion = 1
			else :
				continue

		#Movimiento hacia la izquierda.
		if entrada.upper()=="A":
			if x==0:
				continue
			elif Lab[y][x-1]==3:
				Lab[y][x-1] = 'O'
				Lab[y][x]= 3
				x=x-1
				historial= historial+1

#Menú de reinicio o salida.
	try:
		print(f"Movimientos: {historial}")
		ask = input("Has ganado, desea reiniciar o salir? (r/s): ")
		if ask.upper() == "R" :
			opcion = 2
		elif ask.upper()== "S":
			opcion = 3
		else:
			pass
	except ValueError:
		print("Ingresa un valor válido")
	return opcion


#bloque principal

opcion=0
while opcion!=3:
	try:
		n=int(input("Ingrese tamaño del tablero "))
	except ValueError:
		print("Ingrese un valor numérico")
		opcion = 3
	laberinto = Maze(n)
	GeneracionLaberinto(laberinto)
	for y in range(n):
		for x in range(n):
			if laberinto[y][x]==3:
				Alternativos(laberinto,n,x,y)
	ImprimirMap(laberinto,n)
	opcion= Move(laberinto,n)
print("Fin del juego")
